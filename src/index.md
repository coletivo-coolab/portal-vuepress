---
home: true
heroImage: ./baner2.png
logoImage: ./logo.png
tagline: 
actionText: Quick Start →
actionLink: /guide/
features:
  - title: Historia da Comunidade
    details: Saiba mas sobre a rede.
    icone: ./icone.png
  - title: Biblioteca Virtual
    details: Aplicativos e seviços locais
    icone: ./icone2.png
  - title: Faça parte
    details: Seja mais um nó dessa rede
    icone: ./icone3.png
footer: Feito por Coolab com ❤️
---
